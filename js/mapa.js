var map = L.map('map').setView([16.505, -0.19], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/*
if ( khbkj = true){
    marker =L.marker([oxxo[i].lat,oxxo[i].lng], {icon:cerveza}).addTo(map)
}
else{
    marker = L.marker([oxxo[i].lat,oxxo[i].lng]).addTo(map);
}

return marker; */


/*ICONO CERVERVA*/
var cerveza = L.icon({
    iconUrl: 'cerveza.png',

    iconSize:     [70, 70], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

/*------------------*/

oxxo = [
    {
        'name': 'Oxxo Concordia Campeche',
        'lat': 19.8342089,
        'lng': -90.5008639,
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        'name': 'OXXO Independencia',
        'lat': 19.8462909,
        'lng': -90.5365785,
        "allday": true,
        "beer": false,
        "parking": false
    },
    {
        'name': 'OXXO PITAHAYA',
        'lat': 19.83028054,
        'lng': -90.53123434,
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        'name': 'OXXO 3 PODERES',
        'lat': 19.8359038,
        'lng': -90.5220144,
        "allday": true,
        "beer": true,
        "parking": true
    }, 
    {
        'name': 'OXXO Casa De Justicia',
        'lat': 19.8244161,
        'lng': -90.5268001,
        "allday": false,
        "beer": false,
        "parking": true
    },
    {
        'name': 'Oxxo Fracciorama',
        'lat': 19.828423,
        'lng': -90.5347222,
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        'name': 'Oxxo CATASTRO',
        'lat': 19.8487524,
        'lng': -90.5358306,
        "allday": false,
        "beer": false,
        "parking": true
    },
     {
        'name': 'Oxxo San Rafael',
        'lat': 19.8137996,
        'lng': -90.5294478,
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        'name': 'Oxxo Calle 10',
        'lat': 19.845972,
        'lng': -90.53671,
        "allday": false,
        "beer": false,
        "parking": false
    },
    {
        'name': 'Oxxo Resurgimiento',
        'lat': 19.8318888,
        'lng': -90.5586464,
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        'name': 'Oxxo Instituto Campechano',
        'lat': 19.8438171,
        'lng': -90.5403808,
        "allday": true,
        "beer": false,
        "parking": false
    },
    {
        'name': 'Oxxo Cooperativa Kalá',
        'lat': 19.8549731,
        'lng': -90.4805955,
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        'name': 'oxxo Lerma',
        'lat': 19.8110012,
        'lng': -90.5940725,
        "allday": true,
        "beer": true,
        "parking": true
    }
]

console.log(oxxo.length)

for(var i = 0; i<oxxo.length; i++){
    console.log(oxxo[i].name)
    L.marker([oxxo[i].lat,oxxo[i].lng], {icon:cerveza}).addTo(map)
    .bindPopup(oxxo[i].name)
    .openPopup();
}